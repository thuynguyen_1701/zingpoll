Feature: Sign In in Zing Poll
  As a zingpoll user, I want to login in Zing Poll so that I can work with Zing Poll fulfill features
  Scenario Outline: User signs in successfully
    Given I navigate to the <website>
    When I see the Home Page appear
    Then I click Sign In Toolbar
    And I see the Sign In popup
    And I type email <email>
    And I type password <password>
    And I click Sign In button
    Then I see the noticed message that I sign in sucessfully
    Examples:
    | website             | email                  | password |
    | http://zingpoll.com | nhongthuy861@gmail.com | 123456   |


  Scenario Outline: User signs in unsuccessfully
    Given I navigate to the <website>
    When I see the Home Page appear
    Then I click Sign In Toolbar
    And I see the Sign In popup
    And I type email <email>
    And I type password <password>
    And I click Sign In button
    Then I see the noticed message that I sign in sucessfully
    Examples:
      | website             | email                  | password |
      | http://zingpoll.com | ""                     | 123456   |
      | http://zingpoll.com | nhongthuy861@gmail.com | ""       |
