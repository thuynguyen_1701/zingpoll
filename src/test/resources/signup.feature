Feature: Sign Up in Zing Poll
  As a zingpoll user, I want to create a new account in Zing Poll so that I can discover Zing Poll more flexible
  Scenario Outline: User signs up successfully
    Given I navigate to the <website>
    When I see the Home Page appear
    Then I click Sign In Toolbar
    And I see the Sign In popup
    Then I click New User checkbox
    And I see the new user information
    Then I type fullname <fullname>
    And I type email <email>
    And I type password <password>
    And I type re-password <repassword>
    And I click Agree Term of Service
    Then I see the noticed message that I sign up sucessfully

    Examples:
    | website             | fullname             | email                  | password | repassword |
    | http://zingpoll.com | Thuy Nguyen Thi Hong | nhongthuy861@gmail.com | 123456   | 123456     |


  Scenario Outline: User signs up successfully
    Given I navigate to the <website>
    When I see the Home Page appear
    Then I click Sign In Toolbar
    And I see the Sign In popup
    Then I click New User checkbox
    And I see the new user information
    Then I type fullname <fullname>
    And I type email <email>
    And I type password <password>
    And I type re-password <repassword>
    And I click Agree Term of Service
    Then I see the noticed message that I sign up sucessfully

    Examples:
      | website             | fullname             | email                  | password | repassword |
      | http://zingpoll.com | Thuy Nguyen Thi Hong | ""                     | 123456   | 123456     |
      | http://zingpoll.com | Thuy Nguyen Thi Hong | ""                     | ""       | ""         |
      | http://zingpoll.com | Thuy Nguyen Thi Hong | nhongthuy861@gmail.com | ""       | ""         |