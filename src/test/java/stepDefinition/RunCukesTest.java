package stepDefinition;

import cucumber.api.CucumberOptions;
import cucumber.api.junit.Cucumber;
import org.junit.runner.RunWith;

/**
 * Created by mac on 10/29/16.
 */
@RunWith(Cucumber.class)
@CucumberOptions(
        features = "src/test/resources/",
        plugin = {"pretty", "json:target/cucumber-report/cucumber.json"},
        tags = {}
)
public class RunCukesTest {

}
