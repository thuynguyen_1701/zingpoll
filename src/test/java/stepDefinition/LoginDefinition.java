package stepDefinition;

import cucumber.api.java.en.Given;
import cucumber.api.java.en.Then;
import cucumber.api.java.en.When;
import org.openqa.selenium.WebDriver;

import java.util.ArrayList;
import java.util.HashMap;

/**
 * Created by mac on 10/29/16.
 */
public class LoginDefinition {


    public static WebDriver driver;


    @Given("^I navigate to the (.+)$")
    public void i_navigate_to_the(String website) throws Throwable {
        driver = Initiation.driver;
        driver.get(website);
        Thread.sleep(3000);
    }

    @When("^I see the Home Page appear$")
    public void i_see_the_home_page_appear() throws Throwable {


    }

    @Then("^I click Sign In button")
    public void i_click_sign_in_button() throws Throwable {

    }

    @Then("^I click New User checkbox$")
    public void i_click_new_user_checkbox() throws Throwable {

    }

    @Then("^I type fullname (.+)$")
    public void i_type_fullname(String fullname) throws Throwable {

    }

    @Then("^I see the Sign In popup$")
    public void i_see_the_sign_in_popup() throws Throwable {

    }

    @Then("^I see the new user information$")
    public void i_see_the_new_user_information() throws Throwable {

    }

    @Then("^I type email (.+)$")
    public void i_type_email(String email) throws Throwable {

    }

    @Then("^I type password (.+)$")
    public void i_type_password(String password) throws Throwable {

    }

    @Then("^I type re-password (.+)$")
    public void i_type_repassword(String repassword) throws Throwable {

    }

    @Then("^I click Agree Term of Service$")
    public void i_click_agree_term_of_service() throws Throwable {

    }

    @Then("^I click Sign In Toolbar$")
    public void i_click_sign_in_toolbar() throws Throwable {

    }


    @Then("^I see the noticed message that I sign up sucessfully$")
    public void i_see_the_noticed_message_that_i_sign_up_sucessfully() throws Throwable {

    }


}
